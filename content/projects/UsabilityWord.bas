Attribute VB_Name = "UsabilityWord"
Sub MyZoomIn()
    Dim ZP As Integer
    Const MAX_ZOOM = 500
    ZP = Int(ActiveWindow.ActivePane.View.Zoom.Percentage * 1.1)
    If ZP > MAX_ZOOM Then ZP = MAX_ZOOM
    ActiveWindow.ActivePane.View.Zoom.Percentage = ZP
End Sub

Sub MyZoomOut()
    Dim ZP As Integer
    Const MIN_ZOOM = 80
    ZP = Int(ActiveWindow.ActivePane.View.Zoom.Percentage * 0.9)
    If ZP < MIN_ZOOM Then ZP = MIN_ZOOM
    ActiveWindow.ActivePane.View.Zoom.Percentage = ZP
End Sub

Sub DatumEinfügen()
Attribute DatumEinfügen.VB_ProcData.VB_Invoke_Func = "Normal.NewMacros.DatumEinfügen"
'
' DatumEinfügen Makro
'
'
    Selection.InsertDateTime DateTimeFormat:="dd.MM.yyyy", InsertAsField:= _
        False, DateLanguage:=wdGerman, CalendarType:=wdCalendarWestern, _
        InsertAsFullWidth:=False
End Sub

Sub SaveAndExportPDF()
'
' SaveAndExportPDF Makro
'
'
    
    Dim PDFName As String
    PDFName = ActiveDocument.FullName
    PDFName = Mid(PDFName, 1, Len(PDFName) - 5) + ".pdf"
    PDFName = GetDocLocalPath(PDFName)
    
    ActiveDocument.ExportAsFixedFormat OutputFileName:= _
        PDFName _
        , ExportFormat:=wdExportFormatPDF, OpenAfterExport:=False, OptimizeFor:= _
        wdExportOptimizeForPrint, Range:=wdExportAllDocument, From:=1, To:=1, _
        Item:=wdExportDocumentContent, IncludeDocProps:=True, KeepIRM:=True, _
        CreateBookmarks:=wdExportCreateNoBookmarks, DocStructureTags:=True, _
        BitmapMissingFonts:=True, UseISO19005_1:=True
    ActiveDocument.Save
End Sub

Sub SpeichernWithDialogs()
' Speichert das Dokument
' Wenn noch keine Verzeichnis angegeben ist (neues Dokument) wird das Verzeichnis und der Name abgefragt.

    If ActiveDocument.Path = "" Then
        verzeichnis = InputBox("Verzeichnisname", "Datei speichern", "")
        If verzeichnis = "" Then Exit Sub
        datei = InputBox("Dateiname", "Datei speichern", "")
        If datei = "" Then Exit Sub
        dateiName = verzeichnis & "/" & datei
        ActiveDocument.SaveAs2 FileName:=dateiName, FileFormat:= _
            wdFormatXMLDocument, LockComments:=False, Password:="", AddToRecentFiles _
            :=True, WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts _
            :=False, SaveNativePictureFormat:=False, SaveFormsData:=False, _
            SaveAsAOCELetter:=False, CompatibilityMode:=15
    End If
    ActiveDocument.Save
End Sub

Private Function GetDocLocalPath(docName As String) As String
'return the local path for doc, which is either already a local document or a document on OneDrive
Const strcOneDrivePart As String = "https://kattenberge-my.sharepoint.com/personal/bjarne_stargardt_gak-buchholz_org/Documents/"
Const strcLocalPart As String = "C:\Users\Bjarne\OneDrive - Gymnasium am Kattenberge"
Dim strRetVal As String, bytSlashPos As Byte
    
    strRetVal = docName
    If Left(docName, Len(strcOneDrivePart)) = strcOneDrivePart Then 'yep, it's the OneDrive path
        'remove the "remote part"
        strRetVal = Mid(docName, Len(strcOneDrivePart))
        'read the "local part" from the registry and concatenate
        strRetVal = strcLocalPart & strRetVal
        strRetVal = Replace(strRetVal, "/", "\") 'slashes in the right direction
        strRetVal = Replace(strRetVal, "%20", " ") 'a space is a space once more
    End If
    GetDocLocalPath = strRetVal
    
End Function
