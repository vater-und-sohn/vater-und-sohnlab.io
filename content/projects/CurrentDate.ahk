﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance Force ;Sicherstellen, dass man nicht 2 Instanzen bekommt sondern ein Reload
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;############ Datümer eingeben ###################
:*:#dd_::
  FormatTime,Datum,,yyyyMMdd
  send, %Datum%_
return

:*:#dt_::
  FormatTime,Datum,,yyyyMMddHHmm
  send, %Datum%_
return

:*:#t ::
  FormatTime,Datum,,HH:mm
  send, %Datum%-->
return

:*:#ds_::
  FormatTime,Datum,,yyyyMMddHHmmss
  send, %Datum%_
return


;############ Verzeichnisnamen ###################
:*:#autostart::
  Send, shell:startup
return

:*:#vge::
  Send, C:\Users\Sohn\Documents\Schule\Sync\2019-20\Geschichte\Halbjahr1
return


;############## VNC #####################################################
:*:#155::
send, C:\Users\Sohn\Desktop\Raum 1.55.vnc
return

:*:#231::
:*:#multi::
   send, C:\Users\Sohn\Desktop\Raum 2.31 (Computerraum_groß).vnc
return


;############ Zoom-Tasten für Excel ###################
#IfWinActive ahk_exe EXCEL.EXE
^NumpadAdd::
  send, {Ctrl down}p{Ctrl up}
return

#IfWinActive ahk_exe EXCEL.EXE
^NumpadSub::
  send, {Ctrl down}m{Ctrl up}
return
