+++
title = "Usabilty bei Sehbehinderungen"
date = 2019-09-14
+++
Der Würfler leidet leider unter der Sehbehinderung vom Sohn. 
Inzwischen versuchen wir die Welt für Sehbehinderte einfacher zu machen.
Ziel ist es mit Shortcuts schnell weiterzukommen.
Ein geniales unverzichtbares Tool ist dabei [Autohotkey](https://ahkde.github.io/docs/AutoHotkey.htm).
Des weiteren gibt es einige Makros für Word und Excel.

## Office-Makros
Anbei unsere Word-Macros
```
Attribute VB_Name = "UsabilityWord"
Sub MyZoomIn()
    Dim ZP As Integer
    Const MAX_ZOOM = 500
    ZP = Int(ActiveWindow.ActivePane.View.Zoom.Percentage * 1.1)
    If ZP > MAX_ZOOM Then ZP = MAX_ZOOM
    ActiveWindow.ActivePane.View.Zoom.Percentage = ZP
End Sub

Sub MyZoomOut()
    Dim ZP As Integer
    Const MIN_ZOOM = 80
    ZP = Int(ActiveWindow.ActivePane.View.Zoom.Percentage * 0.9)
    If ZP < MIN_ZOOM Then ZP = MIN_ZOOM
    ActiveWindow.ActivePane.View.Zoom.Percentage = ZP
End Sub


Sub SaveAndExportPDF()
'
' SaveAndExportPDF Makro
'
'
    
    Dim PDFName As String
    PDFName = ActiveDocument.FullName
    PDFName = Mid(PDFName, 1, Len(PDFName) - 5) + ".pdf"
    PDFName = GetDocLocalPath(PDFName)
    
    ActiveDocument.ExportAsFixedFormat OutputFileName:= _
        PDFName _
        , ExportFormat:=wdExportFormatPDF, OpenAfterExport:=False, OptimizeFor:= _
        wdExportOptimizeForPrint, Range:=wdExportAllDocument, From:=1, To:=1, _
        Item:=wdExportDocumentContent, IncludeDocProps:=True, KeepIRM:=True, _
        CreateBookmarks:=wdExportCreateNoBookmarks, DocStructureTags:=True, _
        BitmapMissingFonts:=True, UseISO19005_1:=True
    ActiveDocument.Save
End Sub

Sub SpeichernWithDialogs()
' Speichert das Dokument
' Wenn noch keine Verzeichnis angegeben ist (neues Dokument) wird das Verzeichnis und der Name abgefragt.

    If ActiveDocument.Path = "" Then
        verzeichnis = InputBox("Verzeichnisname", "Datei speichern", "")
        If verzeichnis = "" Then Exit Sub
        datei = InputBox("Dateiname", "Datei speichern", "")
        If datei = "" Then Exit Sub
        dateiName = verzeichnis & "/" & datei
        ActiveDocument.SaveAs2 FileName:=dateiName, FileFormat:= _
            wdFormatXMLDocument, LockComments:=False, Password:="", AddToRecentFiles _
            :=True, WritePassword:="", ReadOnlyRecommended:=False, EmbedTrueTypeFonts _
            :=False, SaveNativePictureFormat:=False, SaveFormsData:=False, _
            SaveAsAOCELetter:=False, CompatibilityMode:=15
    End If
    ActiveDocument.Save
End Sub

Private Function GetDocLocalPath(docName As String) As String
'return the local path for doc, which is either already a local document or a document on OneDrive
Const strcOneDrivePart As String = "https://kattenberge-my.sharepoint.com/personal/bjarne_stargardt_gak-buchholz_org/Documents/"
Const strcLocalPart As String = "C:\Users\Bjarne\OneDrive - Gymnasium am Kattenberge"
Dim strRetVal As String, bytSlashPos As Byte
    
    strRetVal = docName
    If Left(docName, Len(strcOneDrivePart)) = strcOneDrivePart Then 'yep, it's the OneDrive path
        'remove the "remote part"
        strRetVal = Mid(docName, Len(strcOneDrivePart))
        'read the "local part" from the registry and concatenate
        strRetVal = strcLocalPart & strRetVal
        strRetVal = Replace(strRetVal, "/", "\") 'slashes in the right direction
        strRetVal = Replace(strRetVal, "%20", " ") 'a space is a space once more
    End If
    GetDocLocalPath = strRetVal
    
End Function
```
und Excel
```
Attribute VB_Name = "UsabilityExcel"
Sub MyZoomIn()
Attribute MyZoomIn.VB_ProcData.VB_Invoke_Func = "p\n14"
    ' Strg + p
    Dim ZP As Integer
    Const MAX_ZOOM = 400
    ZP = Int(ActiveWindow.Zoom * 1.1)
    If ZP > MAX_ZOOM Then ZP = MAX_ZOOM
    ActiveWindow.Zoom = ZP
End Sub

Sub MyZoomOut()
Attribute MyZoomOut.VB_ProcData.VB_Invoke_Func = "m\n14"
    ' Strg + m
    Dim ZP As Integer
    Const MIN_ZOOM = 80
    ZP = Int(ActiveWindow.Zoom * 0.9)
    If ZP < MIN_ZOOM Then ZP = MIN_ZOOM
    ActiveWindow.Zoom = ZP
End Sub
```
## Autohotkey
Wir haben folgende Kommandos bei Autohotkey

```
;############ Datümer eingeben ###################
:*:#dd_::
  FormatTime,Datum,,yyyyMMdd
  send, %Datum%_
return

:*:#dt_::
  FormatTime,Datum,,yyyyMMddHHmm
  send, %Datum%_
return

:*:#t ::
  FormatTime,Datum,,HH:mm
  send, %Datum%-->
return

:*:#ds_::
  FormatTime,Datum,,yyyyMMddHHmmss
  send, %Datum%_
return


;############ Verzeichnisnamen ###################
:*:#autostart::
  Send, shell:startup
return

:*:#vge::
  Send, C:\Users\Sohn\Documents\Schule\Sync\2019-20\Geschichte\Halbjahr1
return


;############## VNC #####################################################
:*:#155::
send, C:\Users\Sohn\Desktop\Raum 1.55.vnc
return

:*:#231::
:*:#multi::
   send, C:\Users\Sohn\Desktop\Raum 2.31 (Computerraum_groß).vnc
return


;############ Zoom-Tasten für Excel ###################
#IfWinActive ahk_exe EXCEL.EXE
^NumpadAdd::
  send, {Ctrl down}p{Ctrl up}
return

#IfWinActive ahk_exe EXCEL.EXE
^NumpadSub::
  send, {Ctrl down}m{Ctrl up}
return
```
