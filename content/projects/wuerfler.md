+++
title = "Android app Würfler"
date = 2017-11-01
+++
Der Würfler ist eine kleine App für Android. Man kann bis zu 9 Würfel würfeln lassen.
Jeder Würfel kann Werte von 1 bis 99 würfeln. 
Beides ist konfigurierbar.
Man kann Würfel fixieren (Kniffelmode), so dass sie erstmal unveränderlich sind.
Es ist unsere erste Android-App. 
Die Beta-Version ist online.

![about](/images/qrcode.png)