Attribute VB_Name = "UsabilityExcel"
Sub MyZoomIn()
Attribute MyZoomIn.VB_ProcData.VB_Invoke_Func = "p\n14"
    ' Strg + p
    Dim ZP As Integer
    Const MAX_ZOOM = 400
    ZP = Int(ActiveWindow.Zoom * 1.1)
    If ZP > MAX_ZOOM Then ZP = MAX_ZOOM
    ActiveWindow.Zoom = ZP
End Sub

Sub MyZoomOut()
Attribute MyZoomOut.VB_ProcData.VB_Invoke_Func = "m\n14"
    ' Strg + m
    Dim ZP As Integer
    Const MIN_ZOOM = 80
    ZP = Int(ActiveWindow.Zoom * 0.9)
    If ZP < MIN_ZOOM Then ZP = MIN_ZOOM
    ActiveWindow.Zoom = ZP
End Sub


