+++
title = "Impressum und Datenschutz"
type = "about"
weight = 10
+++
## Impressum
Dies ist eine rein private Seite, die sich auch jeglicher Kommentierung enthält
und daher frei von der Impressumspflicht. Im Sinne der Datensparsamkeit findet 
sich hier nur die [E-Mail](mailto:opensource21@gmail.com).

## Datenschutz
Weder auf dieser Homepage noch in unserer App sammeln wir irgendwelche Daten. 
Die Seite wird als gitlab-Pages von gitlab.com gehostet. 
Eine Erfassung von Daten kann weder bei der Android-Plattform oder gitlab.com ausgeschlossen werden.
