+++
date = "2018-03-24T12:00:00-00:00"
title = "Erstes Release"

+++
Heute ist es endlich soweit und wir haben unsere erste Beta-Version veröffentlicht.
Wir haben uns entschieden einen Beta-Chanel zu machen, da wir keine automatisierten Tests haben.
Das ist eigentlich schon peinlich andererseits kann man die App noch gut per Hand testen und Zeit ist knapp.

Hier der QR-Code für den Playstore

![about](/images/qrcode.png)

