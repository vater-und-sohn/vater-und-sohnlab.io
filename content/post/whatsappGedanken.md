+++
date = "2021-01-15T12:00:00-00:00"
title = "Gedanken zu Whatsapp"

+++

Aktuell gibt es wieder ein große Welle zur WhatsApp. 
Das hat mich motiviert mal was dazu zu schreiben.

Aktuell einen guten [Überblick über die Alternativen von Threema](https://threema.ch/de/messenger-vergleich) zusammen gestellt. Und auch eine starke Warnung zu 
[Telegram vom SWR](https://www.swr3.de/aktuell/deswegen-ist-telegram-keine-alternative-100.html
).

Bei uns halten wir uns von Whats App fern und zwar aus 2 Gründen:

- Ich denke, WhatsApp kann man [nicht praktikabel und gesetzeskonform einsetzen](https://www.pcwelt.de/a/datenschutzbeauftragter-warnt-fast-alle-whatsapp-nutzer-handeln-illegal,3449942). Ich wundere mich immer wie viele Menschen hohe moralische Ansprüche haben, sei es als Veganer, religiöse Menschen oder ... , aber hier 2 Augen zu drücken.
- Viele meinen ich habe nichts zu verbergen. Zum einen gefährdet man mit der Einstellung Menschen, die nicht den Luxus haben, zum anderen erkennen viele vermutlich gar nicht, was man mit ihren Daten alles Tolles machen kann. Im [DLF](https://www.deutschlandfunk.de) konnte man erfahren, wie man mit [Hilfe von APPs z.B. Depressionen erkennt](https://www.deutschlandfunk.de/digitale-psychiatrie-die-app-dein-seelenwaechter.740.de.html?dram:article_id=467100). Wenn man sich anschaut auf welchen Daten es beruht, dann sind es genau die Daten die viele WhatsApp frei Haus liefern. Die Nutzung durch WhatsApp ist durch die Einwilligung legal. Und auf einmal bekommt das eigene Kind keinen Arbeitsplatz, weil ein Algorithmus die Wahrscheinlichkeit einer psychischen Erkrankung auf 80 % schätzt.

Wer die Wahl hat, hat die Qual. Was soll man denn Alternativ nehmen? Wir sind bei Threema und Signal angemeldet, die beide sehr gut sind. Ich empfehle den meisten Signal, da man bei Threema sich um die Sicherung seiner Identität kümmern muss. Bei Signal muss man nur die Handy-Nummer behalten. Das fällt den meisten leichter. Und wer Angst hat abgeschnitten zu werden von allen Informationen, der kann ja WhatsApp installieren, aber den Zugriff aufs Telefonbuch verweigern. Unbequem, aber legal und nach und nach werden es hoffentlich mehr Signal-Gruppen ;-)
