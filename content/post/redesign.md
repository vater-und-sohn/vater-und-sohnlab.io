+++
date = "2018-03-22T16:00:00-00:00"
title = "Redesign"

+++

Heute haben wir einmal uns das Design vorgenommen. Statt "lebensfrohem" grau, 
haben wir für ein Spiel passendere Farben gewählt, so dass es ansprechender aussieht.
Dabei konnte man dann auch gleich lernen warum es keine gute Idee ist Farben durch
einen int-Wert zu definieren. Die Farben in den Resourcendateien haben auch einen
int-Wert, aber der muss erst konvertiert werden :-0.
