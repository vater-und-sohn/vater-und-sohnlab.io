+++
date = "2018-09-15T12:00:00-00:00"
title = "Update Oreo"

+++
Leider klappt es zur Zeit nicht so mit dem regelmäßigen Updates,
aber zumindest eine Unterstützung für Android 8 und die lang gezogenen Handies
soll es heute geben.
Und die App braucht weniger Platz :-)

Hier der QR-Code für den Playstore

![about](/images/qrcode.png)

